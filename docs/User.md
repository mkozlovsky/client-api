# openapi.model.User

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userId** | **String** |  | [optional] [default to null]
**email** | **String** |  | [optional] [default to null]
**phone** | **String** |  | [optional] [default to null]
**picture** | **String** |  | [optional] [default to null]
**nickname** | **String** |  | [optional] [default to null]
**createdAt** | [**DateTime**](DateTime.md) |  | [optional] [default to null]
**updatedAt** | [**DateTime**](DateTime.md) |  | [optional] [default to null]
**emailVerified** | **bool** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


