# openapi.model.AnswerRadio

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**values** | [**List&lt;AnswerRadioValues&gt;**](AnswerRadioValues.md) |  | [default to []]
**type** | **String** | selectable answer type, can be radio or checkbox. Radio can select only one choice while checkbox can have multiple choises selected | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


