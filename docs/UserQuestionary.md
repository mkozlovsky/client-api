# openapi.model.UserQuestionary

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] [default to null]
**user** | **String** |  | [optional] [default to null]
**questionary** | **String** |  | [optional] [default to null]
**answers** | [**List&lt;UserAnswer&gt;**](UserAnswer.md) |  | [optional] [default to []]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


