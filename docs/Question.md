# openapi.model.Question

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [default to null]
**title** | **String** |  | [default to null]
**answers** | [**List&lt;AnyOf&lt;AnswerRadio,AnswerMetter&gt;&gt;**](AnyOf&lt;AnswerRadio,AnswerMetter&gt;.md) |  | [default to []]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


