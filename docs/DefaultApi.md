# openapi.api.DefaultApi

## Load the API package
```dart
import 'package:openapi/api.dart';
```

All URIs are relative to *https://virtserver.swaggerhub.com/life-tech/Leader-api/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**questionGet**](DefaultApi.md#questionGet) | **GET** /question | Questions List
[**questionPost**](DefaultApi.md#questionPost) | **POST** /question | 
[**questionQuestionIdDelete**](DefaultApi.md#questionQuestionIdDelete) | **DELETE** /question/{questionId} | Delete question
[**questionQuestionIdGet**](DefaultApi.md#questionQuestionIdGet) | **GET** /question/{questionId} | Get Question
[**questionaryGet**](DefaultApi.md#questionaryGet) | **GET** /questionary | List of questionaries
[**questionaryPost**](DefaultApi.md#questionaryPost) | **POST** /questionary | Create new questionary
[**questionaryQuestionaryIdStartUserIdGet**](DefaultApi.md#questionaryQuestionaryIdStartUserIdGet) | **GET** /questionary/{questionaryId}/start/{userId} | Start questionary for user
[**usersGet**](DefaultApi.md#usersGet) | **GET** /users | Your GET endpoint
[**usersPost**](DefaultApi.md#usersPost) | **POST** /users | Add new user to system


# **questionGet**
> List<Question> questionGet()

Questions List

List of questions

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: Auth0Token
//defaultApiClient.getAuthentication<HttpBasicAuth>('Auth0Token').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('Auth0Token').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.questionGet();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->questionGet: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List<Question>**](Question.md)

### Authorization

[Auth0Token](../README.md#Auth0Token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **questionPost**
> questionPost()



Create new question

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: Auth0Token
//defaultApiClient.getAuthentication<HttpBasicAuth>('Auth0Token').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('Auth0Token').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    api_instance.questionPost();
} catch (e) {
    print("Exception when calling DefaultApi->questionPost: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[Auth0Token](../README.md#Auth0Token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **questionQuestionIdDelete**
> questionQuestionIdDelete(questionId)

Delete question

### Example 
```dart
import 'package:openapi/api.dart';

var api_instance = DefaultApi();
var questionId = questionId_example; // String | id of question to retrieve

try { 
    api_instance.questionQuestionIdDelete(questionId);
} catch (e) {
    print("Exception when calling DefaultApi->questionQuestionIdDelete: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **questionId** | **String**| id of question to retrieve | [default to null]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **questionQuestionIdGet**
> Question questionQuestionIdGet(questionId)

Get Question

### Example 
```dart
import 'package:openapi/api.dart';

var api_instance = DefaultApi();
var questionId = questionId_example; // String | id of question to retrieve

try { 
    var result = api_instance.questionQuestionIdGet(questionId);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->questionQuestionIdGet: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **questionId** | **String**| id of question to retrieve | [default to null]

### Return type

[**Question**](Question.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **questionaryGet**
> List<Questionary> questionaryGet()

List of questionaries

### Example 
```dart
import 'package:openapi/api.dart';

var api_instance = DefaultApi();

try { 
    var result = api_instance.questionaryGet();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->questionaryGet: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List<Questionary>**](Questionary.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **questionaryPost**
> questionaryPost(questionary)

Create new questionary

### Example 
```dart
import 'package:openapi/api.dart';

var api_instance = DefaultApi();
var questionary = Questionary(); // Questionary | 

try { 
    api_instance.questionaryPost(questionary);
} catch (e) {
    print("Exception when calling DefaultApi->questionaryPost: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **questionary** | [**Questionary**](Questionary.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **questionaryQuestionaryIdStartUserIdGet**
> questionaryQuestionaryIdStartUserIdGet(questionaryId, userId)

Start questionary for user

### Example 
```dart
import 'package:openapi/api.dart';

var api_instance = DefaultApi();
var questionaryId = questionaryId_example; // String | id of questionary to Start
var userId = userId_example; // String | id of user

try { 
    api_instance.questionaryQuestionaryIdStartUserIdGet(questionaryId, userId);
} catch (e) {
    print("Exception when calling DefaultApi->questionaryQuestionaryIdStartUserIdGet: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **questionaryId** | **String**| id of questionary to Start | [default to null]
 **userId** | **String**| id of user | [default to null]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **usersGet**
> List<User> usersGet()

Your GET endpoint

Returns list of users assigned to authorized leader

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: Auth0Token
//defaultApiClient.getAuthentication<HttpBasicAuth>('Auth0Token').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('Auth0Token').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.usersGet();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->usersGet: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List<User>**](User.md)

### Authorization

[Auth0Token](../README.md#Auth0Token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **usersPost**
> usersPost(inlineObject)

Add new user to system

Create new user by leader, after this operation user will be created, activated and verified.

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: Auth0Token
//defaultApiClient.getAuthentication<HttpBasicAuth>('Auth0Token').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('Auth0Token').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var inlineObject = InlineObject(); // InlineObject | 

try { 
    api_instance.usersPost(inlineObject);
} catch (e) {
    print("Exception when calling DefaultApi->usersPost: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inlineObject** | [**InlineObject**](InlineObject.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[Auth0Token](../README.md#Auth0Token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

