# openapi
API for mobile application for leaders and administrators

This Dart package is automatically generated by the [OpenAPI Generator](https://openapi-generator.tech) project:

- API version: 1.0.0
- Build package: org.openapitools.codegen.languages.DartClientCodegen

## Requirements

Dart 2.0 or later

## Installation & Usage

### Github
If this Dart package is published to Github, add the following dependency to your pubspec.yaml
```
dependencies:
  openapi:
    git: https://github.com/GIT_USER_ID/GIT_REPO_ID.git
```

### Local
To use the package in your local drive, add the following dependency to your pubspec.yaml
```
dependencies:
  openapi:
    path: /path/to/openapi
```

## Tests

TODO

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```dart
import 'package:openapi/api.dart';

// TODO Configure HTTP basic authorization: Auth0Token
//defaultApiClient.getAuthentication<HttpBasicAuth>('Auth0Token').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('Auth0Token').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try {
    var result = api_instance.questionGet();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->questionGet: $e\n");
}

```

## Documentation for API Endpoints

All URIs are relative to *https://virtserver.swaggerhub.com/life-tech/Leader-api/1.0.0*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*DefaultApi* | [**questionGet**](docs//DefaultApi.md#questionget) | **GET** /question | Questions List
*DefaultApi* | [**questionPost**](docs//DefaultApi.md#questionpost) | **POST** /question | 
*DefaultApi* | [**questionQuestionIdDelete**](docs//DefaultApi.md#questionquestioniddelete) | **DELETE** /question/{questionId} | Delete question
*DefaultApi* | [**questionQuestionIdGet**](docs//DefaultApi.md#questionquestionidget) | **GET** /question/{questionId} | Get Question
*DefaultApi* | [**questionaryGet**](docs//DefaultApi.md#questionaryget) | **GET** /questionary | List of questionaries
*DefaultApi* | [**questionaryPost**](docs//DefaultApi.md#questionarypost) | **POST** /questionary | Create new questionary
*DefaultApi* | [**questionaryQuestionaryIdStartUserIdGet**](docs//DefaultApi.md#questionaryquestionaryidstartuseridget) | **GET** /questionary/{questionaryId}/start/{userId} | Start questionary for user
*DefaultApi* | [**usersGet**](docs//DefaultApi.md#usersget) | **GET** /users | Your GET endpoint
*DefaultApi* | [**usersPost**](docs//DefaultApi.md#userspost) | **POST** /users | Add new user to system


## Documentation For Models

 - [AnswerMetter](docs//AnswerMetter.md)
 - [AnswerRadio](docs//AnswerRadio.md)
 - [AnswerRadioValues](docs//AnswerRadioValues.md)
 - [InlineObject](docs//InlineObject.md)
 - [Question](docs//Question.md)
 - [Questionary](docs//Questionary.md)
 - [User](docs//User.md)
 - [UserAnswer](docs//UserAnswer.md)
 - [UserQuestionary](docs//UserQuestionary.md)


## Documentation For Authorization


## Auth0Token

- **Type**: HTTP basic authentication


## Author




