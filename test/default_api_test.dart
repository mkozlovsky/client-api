import 'package:openapi/api.dart';
import 'package:test/test.dart';


/// tests for DefaultApi
void main() {
  var instance = DefaultApi();

  group('tests for DefaultApi', () {
    // Questions List
    //
    // List of questions
    //
    //Future<List<Question>> questionGet() async 
    test('test questionGet', () async {
      // TODO
    });

    // Create new question
    //
    //Future questionPost() async 
    test('test questionPost', () async {
      // TODO
    });

    // Delete question
    //
    //Future questionQuestionIdDelete(String questionId) async 
    test('test questionQuestionIdDelete', () async {
      // TODO
    });

    // Get Question
    //
    //Future<Question> questionQuestionIdGet(String questionId) async 
    test('test questionQuestionIdGet', () async {
      // TODO
    });

    // List of questionaries
    //
    //Future<List<Questionary>> questionaryGet() async 
    test('test questionaryGet', () async {
      // TODO
    });

    // Create new questionary
    //
    //Future questionaryPost({ Questionary questionary }) async 
    test('test questionaryPost', () async {
      // TODO
    });

    // Start questionary for user
    //
    //Future questionaryQuestionaryIdStartUserIdGet(String questionaryId, String userId) async 
    test('test questionaryQuestionaryIdStartUserIdGet', () async {
      // TODO
    });

    // Your GET endpoint
    //
    // Returns list of users assigned to authorized leader
    //
    //Future<List<User>> usersGet() async 
    test('test usersGet', () async {
      // TODO
    });

    // Add new user to system
    //
    // Create new user by leader, after this operation user will be created, activated and verified.
    //
    //Future usersPost({ InlineObject inlineObject }) async 
    test('test usersPost', () async {
      // TODO
    });

  });
}
