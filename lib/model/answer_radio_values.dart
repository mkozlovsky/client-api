part of openapi.api;

class AnswerRadioValues {
  
  String id = null;
  
  String text = null;
  AnswerRadioValues();

  @override
  String toString() {
    return 'AnswerRadioValues[id=$id, text=$text, ]';
  }

  AnswerRadioValues.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    id = json['id'];
    text = json['text'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (id != null)
      json['id'] = id;
    if (text != null)
      json['text'] = text;
    return json;
  }

  static List<AnswerRadioValues> listFromJson(List<dynamic> json) {
    return json == null ? List<AnswerRadioValues>() : json.map((value) => AnswerRadioValues.fromJson(value)).toList();
  }

  static Map<String, AnswerRadioValues> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, AnswerRadioValues>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = AnswerRadioValues.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of AnswerRadioValues-objects as value to a dart map
  static Map<String, List<AnswerRadioValues>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<AnswerRadioValues>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = AnswerRadioValues.listFromJson(value);
       });
     }
     return map;
  }
}

