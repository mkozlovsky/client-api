part of openapi.api;

class AnswerMetter {
  
  String type = null;
  //enum typeEnum {  date,  time,  datetime,  number,  };{
  
  int min = null;
  
  int max = null;
  AnswerMetter();

  @override
  String toString() {
    return 'AnswerMetter[type=$type, min=$min, max=$max, ]';
  }

  AnswerMetter.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    type = json['type'];
    min = json['min'];
    max = json['max'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (type != null)
      json['type'] = type;
    if (min != null)
      json['min'] = min;
    if (max != null)
      json['max'] = max;
    return json;
  }

  static List<AnswerMetter> listFromJson(List<dynamic> json) {
    return json == null ? List<AnswerMetter>() : json.map((value) => AnswerMetter.fromJson(value)).toList();
  }

  static Map<String, AnswerMetter> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, AnswerMetter>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = AnswerMetter.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of AnswerMetter-objects as value to a dart map
  static Map<String, List<AnswerMetter>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<AnswerMetter>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = AnswerMetter.listFromJson(value);
       });
     }
     return map;
  }
}

