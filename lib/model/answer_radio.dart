part of openapi.api;

class AnswerRadio {
  
  List<AnswerRadioValues> values = [];
  /* selectable answer type, can be radio or checkbox. Radio can select only one choice while checkbox can have multiple choises selected */
  String type = null;
  //enum typeEnum {  radio,  checkbox,  };{
  AnswerRadio();

  @override
  String toString() {
    return 'AnswerRadio[values=$values, type=$type, ]';
  }

  AnswerRadio.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    values = (json['values'] == null) ?
      null :
      AnswerRadioValues.listFromJson(json['values']);
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (values != null)
      json['values'] = values;
    if (type != null)
      json['type'] = type;
    return json;
  }

  static List<AnswerRadio> listFromJson(List<dynamic> json) {
    return json == null ? List<AnswerRadio>() : json.map((value) => AnswerRadio.fromJson(value)).toList();
  }

  static Map<String, AnswerRadio> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, AnswerRadio>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = AnswerRadio.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of AnswerRadio-objects as value to a dart map
  static Map<String, List<AnswerRadio>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<AnswerRadio>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = AnswerRadio.listFromJson(value);
       });
     }
     return map;
  }
}

