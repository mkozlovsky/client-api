part of openapi.api;

class Question {
  
  String id = null;
  
  String title = null;
  
  List<AnyOf<AnswerRadio,AnswerMetter>> answers = [];
  Question();

  @override
  String toString() {
    return 'Question[id=$id, title=$title, answers=$answers, ]';
  }

  Question.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    id = json['id'];
    title = json['title'];
    answers = (json['answers'] == null) ?
      null :
      AnyOf&lt;AnswerRadio,AnswerMetter&gt;.listFromJson(json['answers']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (id != null)
      json['id'] = id;
    if (title != null)
      json['title'] = title;
    if (answers != null)
      json['answers'] = answers;
    return json;
  }

  static List<Question> listFromJson(List<dynamic> json) {
    return json == null ? List<Question>() : json.map((value) => Question.fromJson(value)).toList();
  }

  static Map<String, Question> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, Question>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = Question.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of Question-objects as value to a dart map
  static Map<String, List<Question>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<Question>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = Question.listFromJson(value);
       });
     }
     return map;
  }
}

