part of openapi.api;

class UserAnswer {
  /* Question id */
  String question = null;
  
  String value = null;
  UserAnswer();

  @override
  String toString() {
    return 'UserAnswer[question=$question, value=$value, ]';
  }

  UserAnswer.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    question = json['question'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (question != null)
      json['question'] = question;
    if (value != null)
      json['value'] = value;
    return json;
  }

  static List<UserAnswer> listFromJson(List<dynamic> json) {
    return json == null ? List<UserAnswer>() : json.map((value) => UserAnswer.fromJson(value)).toList();
  }

  static Map<String, UserAnswer> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, UserAnswer>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = UserAnswer.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of UserAnswer-objects as value to a dart map
  static Map<String, List<UserAnswer>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<UserAnswer>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = UserAnswer.listFromJson(value);
       });
     }
     return map;
  }
}

