part of openapi.api;

class Questionary {
  
  String id = null;
  
  String title = null;
  
  List<Question> questions = [];
  Questionary();

  @override
  String toString() {
    return 'Questionary[id=$id, title=$title, questions=$questions, ]';
  }

  Questionary.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    id = json['id'];
    title = json['title'];
    questions = (json['questions'] == null) ?
      null :
      Question.listFromJson(json['questions']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (id != null)
      json['id'] = id;
    if (title != null)
      json['title'] = title;
    if (questions != null)
      json['questions'] = questions;
    return json;
  }

  static List<Questionary> listFromJson(List<dynamic> json) {
    return json == null ? List<Questionary>() : json.map((value) => Questionary.fromJson(value)).toList();
  }

  static Map<String, Questionary> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, Questionary>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = Questionary.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of Questionary-objects as value to a dart map
  static Map<String, List<Questionary>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<Questionary>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = Questionary.listFromJson(value);
       });
     }
     return map;
  }
}

