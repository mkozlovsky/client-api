part of openapi.api;

class User {
  
  String userId = null;
  
  String email = null;
  
  String phone = null;
  
  String picture = null;
  
  String nickname = null;
  
  DateTime createdAt = null;
  
  DateTime updatedAt = null;
  
  bool emailVerified = null;
  User();

  @override
  String toString() {
    return 'User[userId=$userId, email=$email, phone=$phone, picture=$picture, nickname=$nickname, createdAt=$createdAt, updatedAt=$updatedAt, emailVerified=$emailVerified, ]';
  }

  User.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    userId = json['user_id'];
    email = json['email'];
    phone = json['phone'];
    picture = json['picture'];
    nickname = json['nickname'];
    createdAt = (json['created_at'] == null) ?
      null :
      DateTime.parse(json['created_at']);
    updatedAt = (json['updated_at'] == null) ?
      null :
      DateTime.parse(json['updated_at']);
    emailVerified = json['email_verified'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (userId != null)
      json['user_id'] = userId;
    if (email != null)
      json['email'] = email;
    if (phone != null)
      json['phone'] = phone;
    if (picture != null)
      json['picture'] = picture;
    if (nickname != null)
      json['nickname'] = nickname;
    if (createdAt != null)
      json['created_at'] = createdAt == null ? null : createdAt.toUtc().toIso8601String();
    if (updatedAt != null)
      json['updated_at'] = updatedAt == null ? null : updatedAt.toUtc().toIso8601String();
    if (emailVerified != null)
      json['email_verified'] = emailVerified;
    return json;
  }

  static List<User> listFromJson(List<dynamic> json) {
    return json == null ? List<User>() : json.map((value) => User.fromJson(value)).toList();
  }

  static Map<String, User> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, User>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = User.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of User-objects as value to a dart map
  static Map<String, List<User>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<User>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = User.listFromJson(value);
       });
     }
     return map;
  }
}

