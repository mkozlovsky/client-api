part of openapi.api;

class UserQuestionary {
  
  String id = null;
  
  String user = null;
  
  String questionary = null;
  
  List<UserAnswer> answers = [];
  UserQuestionary();

  @override
  String toString() {
    return 'UserQuestionary[id=$id, user=$user, questionary=$questionary, answers=$answers, ]';
  }

  UserQuestionary.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    id = json['id'];
    user = json['user'];
    questionary = json['questionary'];
    answers = (json['answers'] == null) ?
      null :
      UserAnswer.listFromJson(json['answers']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (id != null)
      json['id'] = id;
    if (user != null)
      json['user'] = user;
    if (questionary != null)
      json['questionary'] = questionary;
    if (answers != null)
      json['answers'] = answers;
    return json;
  }

  static List<UserQuestionary> listFromJson(List<dynamic> json) {
    return json == null ? List<UserQuestionary>() : json.map((value) => UserQuestionary.fromJson(value)).toList();
  }

  static Map<String, UserQuestionary> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, UserQuestionary>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = UserQuestionary.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of UserQuestionary-objects as value to a dart map
  static Map<String, List<UserQuestionary>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<UserQuestionary>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = UserQuestionary.listFromJson(value);
       });
     }
     return map;
  }
}

